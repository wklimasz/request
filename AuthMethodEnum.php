<?php 

enum AuthMethodEnum {
    CASE BASIC;
    CASE JWT;
    CASE NONE;
}