<?php


class Response{
    private bool $success = false; 

    public function __construct(
        private string $response,
        private int $httpCode
    )
    {
        $this->success = ($this->httpCode >= 200 && $this->httpCode < 400); 
    }

    public function isSuccessful():bool{
        return $this->success; 
    }

    public function isFailed():bool{
        return $this->success; 
    }

    public function json(){
        return json_encode($this->response); 
    }

    public function body():string{
        return $this->response; 
    }

    public function httpCode() : int{
        return $this->httpCode; 
    }
}