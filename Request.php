<?php 

include 'AuthMethodEnum.php';
include 'TypeRequestEnum.php';
include 'Response.php'; 

class Request{
    private AuthMethodEnum $authMethod = AuthMethodEnum::NONE; 
    private $curl; 

    public function __construct(
        public string $url, 
        private TypeRequestEnum $typeRequest = TypeRequestEnum::GET,
        private ?array $body = [], 
        private array $headers = []
    )
    {
        $this->curl =  curl_init();
    }

    public function setHeaders(?array $headers):Request {
        $this->headers = $headers; 
        return $this; 
    }

    public function setBody(?array $body): Request{
        $this->body = $body; 

        return $this; 
    }

    public function authBasic(string $user, string $password):Request {
        $this->authMethod = AuthMethodEnum::BASIC; 

        curl_setopt($this->curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
        curl_setopt($this->curl, CURLOPT_USERPWD, $user . ':' . $password);

        return $this; 
    }

    public function authToken(string $token): Request{
        $this->authMethod = AuthMethodEnum::JWT; 

        array_push($this->headers, 'Authorization: Bearer '.$token); 

        return $this; 
    }

    public function call(): Response {
        if(!empty($this->headers)){
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->headers);
        }

        if($this->typeRequest === TypeRequestEnum::POST){
            curl_setopt($this->curl, CURLOPT_POST, true);
        }
        else if($this->typeRequest === TypeRequestEnum::PUT){
            curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "PUT"); 
            
        }
        else if($this->typeRequest === TypeRequestEnum::DELETE){
            curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "DELETE"); 
        }
        
        
        curl_setopt($this->curl, CURLOPT_URL, $this->url);
        if(!empty($this->body)){
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($this->body));
        }
        
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);

        $output = curl_exec($this->curl);

        $code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

        $response = new Response($output, $code); 

        return $response; 
    }

    public function __destruct(){
        curl_close($this->curl);
    }
}